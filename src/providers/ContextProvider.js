import React, { Component } from 'react';
import Context from './ApplicationContext';
import { cardData } from '../data/data';
export default class ContextProvider extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return(
            <Context.Provider value={cardData}>
                {this.props.children}
            </Context.Provider>
        )
    }
}