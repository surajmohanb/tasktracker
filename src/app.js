import React from "react";
import ReactDOM from "react-dom";
import Home from './components/home';
import ContextProvider from './providers/ContextProvider';
import Context from './providers/ApplicationContext';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
const theme = createMuiTheme({
    typography: {
        useNextVariants: true,
    },
});
ReactDOM.render(
    <ContextProvider>
        <Context.Consumer>
            {data => <MuiThemeProvider theme={theme}><Home {...data} /></MuiThemeProvider>}
        </Context.Consumer>
    </ContextProvider>
    , document.getElementById("root"));